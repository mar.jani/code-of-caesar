package pl.silesiakursy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

// Zadanie 3
// W pliku `Main3.java` umieść  metodę  o sygnaturze `static String upperCase(String str, int index)` .
// Metoda zwraca napis `str`, w którym znaki zostały zamienione na duże z pozycji podzielnych przez
//`index`

public class Main3 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        System.out.println("Podaj tekst:");
        String str = scanner.nextLine();

        System.out.println("Podaj wartość indeksu:");
        int index = scanner.nextInt();
        scanner.nextLine();

        System.out.println(upperCase(str, index));
    }

    static String upperCase(String str, int index) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            if (i % index == 0) {
                stringBuilder.append(str.toUpperCase().charAt(i));
            } else {
                stringBuilder.append(str.charAt(i));
            }
        }
        return stringBuilder.toString();
    }
}
