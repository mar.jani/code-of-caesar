package pl.silesiakursy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

//Zadanie 1
// W pliku `Main1.java` umieść metodę o sygnaturze `static String encode(String str)`.
//1. Uzupełnij ciało metody tak, by zwracała napis zaszyfrowany szyfrem Cezara

public class Main1 {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj tekst do zaszyfrowania:");
        System.out.println(encode(scanner.nextLine()));
    }

    static StringBuilder encode(String str) {
        List<Character> keyList = new ArrayList<>();
        keyList = Arrays.asList('A', 'Ą', 'B', 'C', 'Ć', 'D', 'E', 'Ę', 'F', 'G',
                'H', 'I', 'J', 'K', 'L', 'Ł', 'M', 'N', 'Ń', 'O', 'Ó',
                'P', 'R', 'S', 'Ś', 'T', 'U', 'W', 'Y', 'Z', 'Ź', 'Ż',
                'a', 'ą', 'b', 'c', 'ć', 'd', 'e', 'ę', 'f', 'g', 'h',
                'i', 'j', 'k', 'l', 'Ł', 'm', 'n', 'ń', 'o', 'ó', 'P',
                'r', 's', 'ś', 't', 'u', 'w', 'y', 'z', 'ź', 'ż');

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < str.length(); i++) {
            char charToEncode = str.charAt(i);
            int index = keyList.indexOf(charToEncode);

            if (charToEncode <= 64 || charToEncode >= 91 && charToEncode <= 96) {
                stringBuilder.append(charToEncode);
            } else if (index + 3 > keyList.size()) {
                stringBuilder.append(keyList.get((index + 3) - keyList.size()));
            } else {
                stringBuilder.append(keyList.get(index + 3));
            }
        }
        return stringBuilder;
    }
}
