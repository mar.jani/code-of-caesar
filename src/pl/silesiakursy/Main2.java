package pl.silesiakursy;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

//Zadanie 2
// W pliku `Main2.java` umieść zmodyfikowaną metodę z zadania domowego 1 o sygnaturze
// `static String encode(String str, int shift)`.
//1. `shift` - oznacza przesunięcie znaków
//2. Dodaj metodę deszyfrującą `static String decode(String str, int shift)` .

public class Main2 {
    static List<Character> keyList = new ArrayList<>();

    public static void main(String[] args) {
        keyList = Arrays.asList('A', 'Ą', 'B', 'C', 'Ć', 'D', 'E', 'Ę', 'F', 'G',
                'H', 'I', 'J', 'K', 'L', 'Ł', 'M', 'N', 'Ń', 'O', 'Ó',
                'P', 'R', 'S', 'Ś', 'T', 'U', 'W', 'Y', 'Z', 'Ź', 'Ż',
                'a', 'ą', 'b', 'c', 'ć', 'd', 'e', 'ę', 'f', 'g', 'h',
                'i', 'j', 'k', 'l', 'Ł', 'm', 'n', 'ń', 'o', 'ó', 'P',
                'r', 's', 'ś', 't', 'u', 'w', 'y', 'z', 'ź', 'ż');

        Scanner scanner = new Scanner(System.in);
        System.out.println("Podaj wartość przesunięcia:");
        int shift = scanner.nextInt();
        scanner.nextLine();

        System.out.println("Podaj tekst do zaszyfrowania:");
        int indexUpper = 2;
        String string = encode(scanner.nextLine(), shift, indexUpper);
        System.out.println("Tekst zakodowany: " + string);

        System.out.println("Tekst zdekodowany: " + decode(string, shift));
    }

    static String encode(String str, int shift, int indexUpper) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            char charToEncode = str.charAt(i);
            int index = keyList.indexOf(charToEncode);

            if (isSpecialCharacter(charToEncode)) {
                stringBuilder.append(charToEncode);
            } else if (index + shift > keyList.size()) {
                stringBuilder.append(keyList.get((index + shift) - keyList.size()));
            } else {
                stringBuilder.append(keyList.get(index + shift));
            }
        }
        return stringBuilder.toString();
    }

    static String decode(String str, int shift) {
        StringBuilder stringBuilder = new StringBuilder();

        for (int i = 0; i < str.length(); i++) {
            char charToEncode = str.charAt(i);
            int index = keyList.indexOf(charToEncode);

            if (isSpecialCharacter(charToEncode)) {
                stringBuilder.append(charToEncode);
            } else if (index - shift < 0) {
                stringBuilder.append(keyList.get(keyList.size() - (index - shift)));
            } else {
                stringBuilder.append(keyList.get(index - shift));
            }
        }
        return stringBuilder.toString();
    }

    private static boolean isSpecialCharacter(char charToEncode) {
        return !keyList.contains(charToEncode);
    }


}
